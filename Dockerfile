# SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM alpine:3

COPY conf-stunnel.sh /usr/local/bin/conf-stunnel

RUN apk add --no-cache stunnel

CMD conf-stunnel /etc/ssl/private/ldap/keys /tmp/stunnel/stunnel.conf /etc/stunnel/stunnel.conf && stunnel
