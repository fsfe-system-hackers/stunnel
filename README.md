<!--
SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# FSFE stunnel for LDAP

This Docker image is being used to allow containers to securely access
the FSFE LDAP server via an encrypted connection via client
certificates.

## Preparations

First of all, create a new LDAP client key and certificate for your
service against the FSFE's CA.

On the Docker host(s), store the required files. The default values are
next to the items:

* a template for the stunnel config: `/srv/ldap/stunnel/stunnel.conf`
* the CA certificate of the LDAP server: `/srv/ldap/keys/ca/ca.crt`
* the LDAP client key and certificate: `/srv/ldap/keys/servicename/client.crt`, `/srv/ldap/keys/servicename/client.key`

## Integration into Docker service

To allow your container to connect to LDAP, follow these steps:

### 1. Add to docker compose file

In your `docker-compose.yml`, add the stunnel image as a service.
Please rename `servicename` accordingly:

```
  servicename-ldap:
    container_name: servicename-ldap
    image: fsfe/stunnel:1
    restart: always
    expose:
      - "389"
    volumes:
      - "/srv/ldap/stunnel:/tmp/stunnel/:ro"
      - "/srv/ldap/keys/ca:/etc/ssl/private/ldap/ca:ro"
      - "/srv/ldap/keys/servicename:/etc/ssl/private/ldap/keys:ro"
```

### 2. Add to webserver config

In your webserver config, add the LDAP authentication. For **Apache**,
this may look like the following:

```
<Directory /var/www/html/>
  AuthType Basic
  AuthName "Access permissions required"
  AuthBasicProvider ldap
  AuthLDAPBindDN uid=groupsreader,ou=fellowship,dc=fsfe,dc=org
  AuthLDAPBindPassword LDAP_BIND_PW
  AuthLDAPUrl ldap://servicename-ldap:389/ou=fellowship,dc=fsfe,dc=org?uid?sub?(!(uid=*@*))
  Require ldap-group cn=team, ou=groups, dc=fsfe, dc=org
</Directory>
```

Again, make sure to replace `servicename` with the actual name of the
container. Also, do not use any encryption for LDAP inside the service,
stunnel takes care of that. Please be aware that providing a password
is still required in our setup.

### 3. Activate webserver modules

Depending on your webserver, you might have to enable LDAP modules.
It's best to do that in your Dockerfile.

* Apache: `RUN a2enmod ldap authnz_ldap`
